#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2023
#
# SPDX-License-Identifier: MIT

require linux-cip-common.inc

KERNEL_DEFCONFIG_VERSION ?= "6.1.y-cip"

SRC_URI[sha256sum] = "5e4dd386aa8a4cce341b8efeec76320feed53770ead67c838ffb321edd47b157"
