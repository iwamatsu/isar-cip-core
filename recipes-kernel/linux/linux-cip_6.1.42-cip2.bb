#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2023
#
# SPDX-License-Identifier: MIT

require linux-cip-common.inc

KERNEL_DEFCONFIG_VERSION ?= "6.1.y-cip"

SRC_URI[sha256sum] = "70d05a84e8194a94dccbdce952a983466a404498fe59fb61fd3de1463322bb5a"
