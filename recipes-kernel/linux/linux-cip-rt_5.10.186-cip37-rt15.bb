#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2021-2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-rt-common.inc

KERNEL_DEFCONFIG_VERSION ?= "5.10.y-cip"

SRC_URI[sha256sum] = "754f2dba8d1fa098299007d2b8384e349be0ca7533bda497c98df110d8b95728"
